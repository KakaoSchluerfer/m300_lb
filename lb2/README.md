 
<div style="text-align:left"><img src="./pictures/title.png" /></div>


## Table of Contents
- [Table of Contents](#table-of-contents)
- [1. Introduction](#1-introduction)
  - [1.1 Graphic overview](#11-graphic-overview)
- [2. Installation](#2-installation)
  - [2.1 Clone the Repository](#21-clone-the-repository)
  - [2.2 Create Domain Controller](#22-create-domain-controller)
  - [2.3 Create Windows Client for domain join](#23-create-windows-client-for-domain-join)
- [3. Security Aspects](#3-security-aspects)
- [4. Vagrantfile](#4-vagrantfile)
- [5. PowerShell Scripts](#5-powershell-scripts)
- [6. AD-Users and their functions](#6-ad-users-and-their-functions)
- [7. Service URL](#7-service-url)
  - [7.1 Windows Client `desktop`](#71-windows-client-desktop)
  - [7.2 Windows Domain Controller `dc`](#72-windows-domain-controller-dc)
- [8. Testing](#8-testing)
- [9. Sources](#9-sources)

---
## 1. Introduction

In this document LB2 is documented during the project time. 
The task was to make an Infrastructure as Code project with Vagrant by automating a service or server service. 
I decided to use Vagrant to set up a Virtualbox VM that provides an Active Directory domain environment.

### 1.1 Graphic overview
<div style="text-align:left"><img src="./pictures/network.png" /></div>

---
## 2. Installation

### 2.1 Clone the Repository
```bash
git clone https://gitlab.com/KakaoSchluerfer/m300_lb.git
```

This `Vagrantfile` uses the [`vagrant-reload`](https://github.com/aidanns/vagrant-reload) plugin to reboot the Windows VM's during provisioning. If you don't have this plugin installed, do it now with

```bash
vagrant plugin install vagrant-reload
```

To build the boxes, use `vagrant up` with the box name.
Each box will be reboot twice until all features are up and running.


### 2.2 Create Domain Controller
First create the AD domain controller

```bash
vagrant up dc
```

After that, the domain `windomain.local` is up and running at IP address `192.168.38.2`.


### 2.3 Create Windows Client for domain join

```bash
vagrant up desktop
```

--- 

## 3. Security Aspects

A new AD user ``s2-join-domain`` is created with the fill-ad.ps1 script

---

## 4. Vagrantfile


<details>
<summary>Vagrant Box `DC` Domain Controller</summary>
<br>

```bash
config.vm.define "dc" do |config|
    ### Declaration of Vagrant Box and VM Hostname
    config.vm.box = "gusztavvargadr/windows-server"
    config.vm.hostname = "dc"
    
    ### Communication over WinRM
    config.winrm.transport = :plaintext
    config.winrm.basic_auth_only = true
    config.vm.communicator = "winrm"

    ### Port Forwarding SSH/RDP/WINRM, Set IP/Gateway of Host-Only Net-Adapter
    config.vm.network :forwarded_port, guest: 5985, host: 5985, id: "winrm", auto_correct: true
    config.vm.network :forwarded_port, guest: 22, host: 2222, id: "ssh", auto_correct: true
    config.vm.network :forwarded_port, guest: 3389, host: 33891, id: "rdp", auto_correct: true
    config.vm.network :private_network, ip: "192.168.38.2", gateway: "192.168.38.1"
    
    ### Provisioning with PowerShell Scripts
    config.vm.provision "shell", path: "scripts/fix-second-network.ps1", privileged: false, args: "192.168.38.2"
    config.vm.provision "shell", path: "scripts/ena-autologin.ps1", privileged: false
    config.vm.provision "shell", path: "scripts/provision.ps1", privileged: false
    
    ### Reload Twice to ensure AD-Service works fine
    config.vm.provision "reload"
    config.vm.provision "shell", path: "scripts/fill-ad.ps1", privileged: false
    config.vm.provision "reload"

    ### Provider Settings for Virtualbox Hypervisor
    config.vm.provider "virtualbox" do |vb, override|
      vb.gui = true
      vb.customize ["modifyvm", :id, "--memory", 4196]
      vb.customize ["modifyvm", :id, "--cpus", 2]
      vb.customize ["modifyvm", :id, "--vram", "32"]
      vb.customize ["modifyvm", :id, "--clipboard", "bidirectional"]
      vb.customize ["setextradata", "global", "GUI/SuppressMessages", "all" ]
    end
  end
```

</details>

<details>
<summary>Vagrant Box `Desktop` Windows 10 Client</summary>
<br>

```bash
  config.vm.define "desktop" do |config|

    ### Declaration of Vagrant Box and VM Hostname
    config.vm.box = "kakaoschluerfer/windows10"
    config.vm.hostname = "desktop"
    
    ### Communication over WinRM
    config.winrm.transport = :plaintext
    config.winrm.basic_auth_only = true
    config.vm.communicator = "winrm"
    
    ### Port Forwarding SSH/RDP/WINRM, Set IP/Gateway of Host-Only Net-Adapter
    config.vm.network :forwarded_port, guest: 5985, host: 5985, id: "winrm", auto_correct: true
    config.vm.network :forwarded_port, guest: 22, host: 2223, id: "ssh", auto_correct: true
    config.vm.network :forwarded_port, guest: 3389, host: 33892, id: "rdp", auto_correct: true
    config.vm.network :private_network, ip: "192.168.38.15", gateway: "192.168.38.1"
    
    ### Provisioning with PowerShell Scripts
    config.vm.provision "shell", path: "scripts/fix-second-network.ps1", privileged: false, args: "-ip 192.168.38.15 -dns 192.168.38.2"
    config.vm.provision "shell", path: "scripts/ena-autologin.ps1", privileged: false
    config.vm.provision "shell", path: "scripts/provision.ps1", privileged: false
    config.vm.provision "shell", path: "scripts/ena-firewall.ps1", privileged: false
    config.vm.provision "reload"
    
    ### Provider Settings for Virtualbox Hypervisor
    config.vm.provider "virtualbox" do |vb, override|
      vb.gui = true
      vb.customize ["modifyvm", :id, "--memory", 2048]
      vb.customize ["modifyvm", :id, "--cpus", 2]
      vb.customize ["modifyvm", :id, "--vram", "32"]
      vb.customize ["modifyvm", :id, "--clipboard", "bidirectional"]
      vb.customize ["setextradata", "global", "GUI/SuppressMessages", "all" ]
    end
  end
```

</details>

---

## 5. PowerShell Scripts

All Scripts are located in Folder [`m300_lb/lb2/scripts`](https://gitlab.com/KakaoSchluerfer/m300_lb/-/blob/master/lb2/scripts/)

|UserName|Function|Applied to|
|:--|:--|--:|
|[`provision.ps1`](https://gitlab.com/KakaoSchluerfer/m300_lb/-/blob/master/lb2/scripts/provision.ps1)|Used for the provisioning process|`desktop` / `dc`|
|[`create-domain.ps1`](https://gitlab.com/KakaoSchluerfer/m300_lb/-/blob/master/lb2/scripts/create-domain.ps1)|Adds the AD DS role and creates Domain "Windomain.Local"|`dc`|
|[`join-domain.ps1`](https://gitlab.com/KakaoSchluerfer/m300_lb/-/blob/master/lb2/scripts/join-domain.ps1)|Used for domain join|`desktop`|
|[`fix-second-network.ps1`](https://gitlab.com/KakaoSchluerfer/m300_lb/-/blob/master/lb2/scripts/fix-second-network.ps1)|Sets the defined IP Settings|`desktop` / `dc`|
|[`ena-firewall.ps1`](https://gitlab.com/KakaoSchluerfer/m300_lb/-/blob/master/lb2/scripts/ena-firewall.ps1)|Enables local client firewall (all profiles)|`desktop`|
|[`fill-ad.ps1`](https://gitlab.com/KakaoSchluerfer/m300_lb/-/blob/master/lb2/scripts/fill-ad.ps1)|Creates OU / users / groups on the domain controller|`dc`|
|[`ena-autologin.ps1`](https://gitlab.com/KakaoSchluerfer/m300_lb/-/blob/master/lb2/scripts/ena-autologin.ps1)|Enables Auto-Login with the user "vagrant"|`desktop` / `dc`|
|[`dis-autologin.ps1`](https://gitlab.com/KakaoSchluerfer/m300_lb/-/blob/master/lb2/scripts/dis-autologin.ps1)|Disables Auto-Login|---|

---
## 6. AD-Users and their functions

Users are generated automatically with the script [`fill-ad.ps1`](https://gitlab.com/KakaoSchluerfer/m300_lb/-/blob/master/lb2/scripts/fill-ad.ps1)

The script uses [`users.csv`](https://gitlab.com/KakaoSchluerfer/m300_lb/-/blob/master/lb2/scripts/users.csv) to export the specific user informations for the creation of them.

|UserName|FullName|Function|
|:--|:--|--:|
|kevin.spath|Kevin Spath|Domain User|
|elias.martinelli|Elias Martinelli|Domain User|
|s2-join-domain|ServiceAccount DomainJoin|Domain Admin|

---

## 7. Service URL
To establish a connection to the Virtual Machines, i decided to use SSH and RDP.

---

### 7.1 Windows Client `desktop`

>SSH
```
127.0.0.1:2223
```
> RDP
```
127.0.0.1:33892
```
---
### 7.2 Windows Domain Controller `dc`


>SSH
```
127.0.0.1:2222
```
> RDP
```
127.0.0.1:33891
```
---
## 8. Testing

### 8.1 Domain Controller
> 1. Connect to the domain controller `dc` over ssh with PuTTy.

```
127.0.0.1:2222
```

> 2. Log on to the machine
```
User: > vagrant
Password: > vagrant
```

> 3. Open PowerShell Console
```
C:\Users\vagrant> powershell
```
<div style="text-align:left"><img src="./pictures/powershell.png" /></div>

> 4. Check AD Forest (windomain.local) 
```
PS C:\Users\vagrant> Get-ADForest
```
<div style="text-align:left"><img src="./pictures/adforest.png" /></div>

### Windows 10
> 1. Connect to the Windows Client `desktop` over ssh with PuTTy.

```
127.0.0.1:2223
```

> 2. Log on to the machine
```
User: > vagrant
Password: > vagrant
```

> 3. Open PowerShell Console
```
C:\Users\vagrant> powershell
```

> 4. Check Domain (windomain.local) 
```
PS C:\Users\vagrant> Get-ADdomain | fl Name, DomainMode
```
---

## 9. Sources

- https://github.com/UNT-CAS/Vagrant-AD-Lab
- https://cloudblogs.microsoft.com/industry-blog/en-gb/technetuk/2016/06/08/setting-up-active-directory-via-powershell/
- https://superuser.com/questions/603627/powershell-script-to-fully-automate-joining-the-domain
