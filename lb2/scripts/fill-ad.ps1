Import-Module ActiveDirectory

NEW-ADOrganizationalUnit -name "Developing"
NEW-ADOrganizationalUnit -name "Developers" -path "OU=Developing,DC=windomain,DC=local"

Import-CSV -delimiter ";" c:\vagrant\scripts\users.csv | foreach {
  New-ADUser -SamAccountName $_.SamAccountName -GivenName $_.GivenName -Surname $_.Surname -Name $_.Name `
             -Path "OU=Developers,OU=Developing,DC=windomain,DC=local" `
             -AccountPassword (ConvertTo-SecureString -AsPlainText $_.Password -Force) -Enabled $true
}

New-ADGroup -Name "AD-Developers" -SamAccountName AD-Developers -GroupCategory Security -GroupScope Global -DisplayName "AD-Developers" -Path "OU=Developers,OU=Developing,DC=windomain,DC=local"

# simple ad users
Add-ADGroupMember -Identity AD-Developers -Members kevin.spath
Add-ADGroupMember -Identity AD-Developers -Members elias.martinelli

# Domain Join User
Add-ADGroupMember -Identity AD-Developers -Members s2-join-domain
Add-ADGroupMember -Identity 'Domain Admins' -Members s2-join-domain
Add-ADGroupMember -Identity 'Administrators' -Members s2-join-domain


<#
Get-ADGroup -filter * -properties GroupCategory | ft name,groupcategory
Get-ADGroupt -identity 'Domain Admins'
#>