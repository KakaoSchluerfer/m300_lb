$box = Get-ItemProperty -Path HKLM:SYSTEM\CurrentControlSet\Control\ComputerName\ComputerName -Name "ComputerName"
$box = $box.ComputerName.ToString().ToLower()

if ($env:COMPUTERNAME -imatch 'vagrant') {

  Write-Host 'Hostname is still the original one, skip provisioning for reboot'


  Write-Host -ForegroundColor red 'Hint: vagrant reload' $box '--provision'

} elseif ((gwmi win32_computersystem).partofdomain -eq $false) {

  Write-Host -ForegroundColor red "Ooops, workgroup!"


  if ($env:COMPUTERNAME -imatch 'dc') {
    . c:\vagrant\scripts\create-domain.ps1 192.168.38.2
  } else {
    . c:\vagrant\scripts\join-domain.ps1
  }

  Write-Host -ForegroundColor red 'Hint: vagrant reload' $box '--provision'

} else {

  Write-Host -ForegroundColor green "I am domain joined!"

  Write-Host 'Provisioning after joining domain'

}