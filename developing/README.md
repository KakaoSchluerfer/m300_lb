# Test Infrastructure for AD

The following boxes could be created:

1. `dc` : The Active Directory Domain controller
2. `desktop` : The Windows 10 Pro Client

## Installation

This `Vagrantfile` uses the [`vagrant-reload`](https://github.com/aidanns/vagrant-reload) plugin to reboot the Windows VM's during provisioning. If you don't have this plugin installed, do it now with

```bash
vagrant plugin install vagrant-reload
```

To build the boxes, use `vagrant up` with the box name.
Each box will be reboot twice until all features are up and running.

### Create Domain Controller
First create the AD domain controller

```bash
vagrant up dc
```

### Create Windows Client for domain join

```bash
vagrant up desktop
```

After that the domain `windomain.local` is up and running at IP address `192.168.38.2`.

## Normal Use
After setting up all boxes, you simply can start and stop the boxes, but the
Domain Controller should be started first and stopped last.

```bash
vagrant up dc
vagrant up desktop
vagrant halt desktop
vagrant halt dc
```
