# http://technet.microsoft.com/de-de/library/dd378937%28v=ws.10%29.aspx
# http://blogs.technet.com/b/heyscriptingguy/archive/2013/10/29/powertip-create-an-organizational-unit-with-powershell.aspx

Import-Module ActiveDirectory

NEW-ADOrganizationalUnit -name "Developing"
NEW-ADOrganizationalUnit -name "Developers" -path "OU=Developing,DC=windomain,DC=local"

Import-CSV -delimiter ";" "C:\repository\m300_lb\developing\scripts\users.csv" | foreach {
  New-ADUser -SamAccountName $_.SamAccountName -GivenName $_.GivenName -Surname $_.Surname -Name $_.Name `
             -Path "OU=Developers,OU=Developing,DC=windomain,DC=local" `
             -AccountPassword (ConvertTo-SecureString -AsPlainText $_.Password -Force) -Enabled $true
}

New-ADGroup -Name "AD-Developers" -SamAccountName AD-Developers -GroupCategory Security -GroupScope Global -DisplayName "AD-Developers" -Path "OU=Developers,OU=Developing,DC=windomain,DC=local"


Add-ADGroupMember -Identity AD-Developers -Member mike.hammer
Add-ADGroupMember -Identity AD-Developers -Member john.franklin

