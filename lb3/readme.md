<div style="text-align:left"><img src="./pictures/title.png" /></div>


# Table of Contents
- [Table of Contents](#table-of-contents)
  - [1. Introduction](#1-introduction)
    - [1.1 Graphic overview](#11-graphic-overview)
  - [2. Installation](#2-installation)
    - [2.1 Clone the Repository](#21-clone-the-repository)
    - [2.2 Change to correct Directory](#22-change-to-correct-directory)
    - [2.3 Start Services](#23-start-services)
  - [3. Security Aspects](#3-security-aspects)
  - [4. Merged Docker-Compose Files](#4-merged-docker-compose-files)
    - [4.1 Understanding multiple Compose files🔗](#41-understanding-multiple-compose-files)
  - [5. Files and Folders](#5-files-and-folders)
  - [6. Service Overview](#6-service-overview)
  - [7. Service URL](#7-service-url)
  - [8. Testing](#8-testing)
  - [9. Sources](#9-sources)

---
## 1. Introduction

In this document LB3 is documented during the project time. 
The task was to build an Infrastructure, which includes an accessible service with implemented security aspects. Further details are written [Here](https://tbzedu.sharepoint.com/:b:/r/sites/M300_Documents/Freigegebene%20Dokumente/Leistungsbeurteilung/M300_LB3_Container_1_1.pdf?csf=1&web=1&e=qwrybr) regarding requirements and so on.

In my realization, the service is a cloud storage (owncloud) and for security reasons a reverse proxy (nginx). To get the best user experience out of owncloud, i decided to a use dedicated database (mariadb) and dedicated caching (redis). 

### 1.1 Graphic overview
As you can see in the picture below, i'm using a docker bridge network with the network suffix: 172.16.32.0/24. 

<div style="text-align:left"><img src="./pictures/overview.png" /></div>

---
## 2. Installation

### 2.1 Clone the Repository
```bash
git clone https://gitlab.com/KakaoSchluerfer/m300_lb.git
```

### 2.2 Change to correct Directory

```bash
cd .\m300_lb\lb3\
```

### 2.3 Start Services


```bash
docker-compose up -d
```

After that, the owncloud service `owncloud_server` is accessible over Reverseproxy `owncloud_nginx` at IP address `127.0.0.1:8484`.



--- 

## 3. Security Aspects

A reverse proxy `owncloud_nginx` was implemented, which takes over the client requests for the ``owncloud_server`` and forwards them accordingly.


<div style="text-align:left"; border-radius10px 25%; ><img src="./pictures/reverseproxy.png" /></div>


---

## 4. Merged Docker-Compose Files

### 4.1 Understanding multiple Compose files🔗

By default, Compose reads two files, a docker-compose.yml and an optional docker-compose.override.yml file. By convention, the docker-compose.yml contains your base configuration. The override file, as its name implies, can contain configuration overrides for existing services or entirely new services.

> Source of text: [Share Compose configurations between files and projects](https://docs.docker.com/compose/extends/)



[`docker-compose.yml`](https://gitlab.com/KakaoSchluerfer/m300_lb/-/blob/master/lb3/docker-compose.yml)
<details>
<summary>Docker-Compose File with General Settings</summary>
<br>

```bash
version: "3"

volumes:
  files:
    driver: local
  mysql:
    driver: local
  redis:
    driver: local
  reverseproxy:
    driver: local

services:
  owncloud:
    image: owncloud/server:10.7
    container_name: owncloud_server
    restart: always
    # Expose port 8080 to links (reverseproxy)
    expose:
      - 8080
    links:
      - nginx
    # owncloud service depends on service mariadb,redis 
    depends_on:
      - mariadb
      - redis
    environment:
      - OWNCLOUD_DOMAIN=${OWNCLOUD_DOMAIN}
      # DB Settings
      - OWNCLOUD_DB_TYPE=mysql
      - OWNCLOUD_DB_NAME=owncloud
      - OWNCLOUD_DB_USERNAME=owncloud
      - OWNCLOUD_DB_PASSWORD=owncloud
      # Define mariadb Host
      - OWNCLOUD_DB_HOST=mariadb
      # Owncloud Credentials (default admin, admin)
      - OWNCLOUD_ADMIN_USERNAME=${ADMIN_USERNAME}
      - OWNCLOUD_ADMIN_PASSWORD=${ADMIN_PASSWORD}
      # UTF8 and Emojis supported
      - OWNCLOUD_MYSQL_UTF8MB4=true
      # Enable dedicated Memory Caching with redis 
      - OWNCLOUD_REDIS_ENABLED=true
      # Define Redis Host
      - OWNCLOUD_REDIS_HOST=redis
    # Checks Healthstatus every 30 Seconds
    healthcheck:
      test: ["CMD", "/usr/bin/healthcheck"]
      interval: 30s
      timeout: 10s
      retries: 5
    volumes:
      - files:/mnt/data

  mariadb:
    image: mariadb:10.5
    container_name: owncloud_mariadb
    restart: always
    # Creates Database for owncloud service
    environment:
      # Set root and owncloud User Password
      - MYSQL_ROOT_PASSWORD=owncloud
      - MYSQL_USER=owncloud
      - MYSQL_PASSWORD=owncloud
      # Create Database for owncloud
      - MYSQL_DATABASE=owncloud
    # Limit max allowed packets to 128MB and log file size to 64MB
    command: ["--max-allowed-packet=128M", "--innodb-log-file-size=64M"]
    # Checks Healthstatus every 10 Seconds
    healthcheck:
      test: ["CMD", "mysqladmin", "ping", "-u", "root", "--password=owncloud"]
      interval: 10s
      timeout: 5s
      retries: 5
    volumes:
      - mysql:/var/lib/mysql

  redis:
    image: redis:6
    container_name: owncloud_redis
    restart: always
    command: ["--databases", "1"]
    # Checks Healthstatus every 10 Seconds
    healthcheck:
      test: ["CMD", "redis-cli", "ping"]
      interval: 10s
      timeout: 5s
      retries: 5
    volumes:
      - redis:/data

  nginx:
    image: nginx:1.19.10
    container_name: owncloud_nginx
    # Gets the local nginx config and writes it onto container 
    volumes:
     - ./templates:/etc/nginx/templates
    # Opens Port 8484, so you can access service from physical host
    ports:
     - "8484:8484"
    # Checks Healthstatus every 90 Seconds
    healthcheck:
      test: ["CMD", "curl", "--fail", "http://127.0.0.1:8484"]
      interval: 1m30s
      timeout: 10s
      retries: 3

```

</details>

[`docker-compose.override.yml`](https://gitlab.com/KakaoSchluerfer/m300_lb/-/blob/master/lb3/docker-compose.override.yml)

<details>
<summary>Docker-Compose File with Network Settings</summary>
<br>

```bash
version: "3"

# Backend Docker Network Bridge defined here
networks:
    back_net:
      driver: bridge
      ipam:
        driver: default
        config:
          - subnet: 172.16.32.0/24
            gateway: 172.16.32.1

services:
  # Sets IP-Adress for owncloud
  owncloud:
    networks:
      back_net:
        ipv4_address: 172.16.32.10
  # Sets IP-Adress for mariadb
  mariadb:
    networks:
      back_net:
        ipv4_address: 172.16.32.20
  # Sets IP-Adress for redis
  redis:
    networks:
      back_net:
        ipv4_address: 172.16.32.30
  # Sets IP-Adress for owncloud
  nginx:
    networks:
     back_net:
       ipv4_address: 172.16.32.40

```

</details>

---
## 5. Files and Folders

Reverseproxy config for nginx service is located here: [`m300_lb/lb3/templates`](https://gitlab.com/KakaoSchluerfer/m300_lb/-/blob/master/lb3/templates/)

[`default.conf.template`](https://gitlab.com/KakaoSchluerfer/m300_lb/-/blob/master/lb3/templates/default.conf.template)

<details>
<summary>Nginx default config</summary>
<br>

```bash
server {
        # Set accessible port for clients
        listen 8484;
        listen [::]:8484;

        # defines log paths
        access_log /var/log/nginx/reverse-access.log;
        error_log /var/log/nginx/reverse-error.log;

        # Pass owncloud webserver with exposed port: 8080
        location / {
                    proxy_pass http://172.16.32.10:8080;
  }
}

```

</details>

---
## 6. Service Overview

|Containername|Service Description|
|:--|:--|
|``owncloud_server``|Cloud Storage System|
|`owncloud_mariadb`|Dedicated Database for ``owncloud_server``|
|`owncloud_redis`|Does the caching for `owncloud_server`|
|`owncloud_nginx`|Nginx Reverseproxy and lets ``owncloud_server`` pass|

---
## 7. Service URL

```
127.0.0.1:8484
```

---
## 8. Testing

1. Start Services
```bash 
docker-compose up -d
```
<div style="text-align:left"><img src="./pictures/start_services.png" /></div>

2. Open any `Web-browser` and open new tab with following address: ``127.0.0.1:8484`` 

<div style="text-align:left"><img src="./pictures/open_tab.png" /></div>

3. Log in with ``Credentials``: 

<div style="text-align:left"><img src="./pictures/credentials.png" /></div>


4. Create new Folder `lb3_testing`

<div style="text-align:left"><img src="./pictures/folder.png" /></div>


5. ``Restart`` Container
```bash 
docker-compose down; docker-compose up -d
```

6. Repeat Step 2-3 and Check if Folder `lb3_testing` still exist  

<div style="text-align:left"><img src="./pictures/check_cli.png" /></div>

<div style="text-align:left"><img src="./pictures/check.png" /></div>



---
## 9. Sources

- https://hub.docker.com/_/owncloud
- https://hub.docker.com/_/redis
- https://hub.docker.com/_/mariadb

